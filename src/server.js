var express = require('express');
var path = require("path");
var bodyParser = require('body-parser');
var mongo = require("mongoose");

var db = mongo.connect("mongodb://localhost:27017/AngularTestDB", function(err, response){
    if(err){ console.log( err); }
    else{ console.log('Connected to database completed'); }
});

var app = express();
app.use(bodyParser());
app.use(bodyParser.json({limit:'5mb'}));
app.use(bodyParser.urlencoded({extended:true}));

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

var Schema = mongo.Schema;

var UsersSchema = new Schema({    
    name: { type: String },
    lastName: { type: String },
    email: { type: String },
    password : { type: String },
}, { versionKey: false});




var model = mongo.model('users', UsersSchema, 'users');

//Initialize data from users

model.find({}).remove()
    .then(() => {
        model.create({
        name: 'Daniel',
        lastName: 'Cruz',
        email: 'dcruz@gmail.com',
        password : 'qwerty123',
    }, {
        name: 'Fernando',
        lastName: 'Casas',
        email: 'fcasas@gmail.com',
        password : 'qwerty123',
    }, {
        name: 'Martin',
        lastName: 'Sanchez',
        email: 'msanchez@hotmail.com',
        password : '123$%^',
    })
    .then(() => console.log('finished populating users'))
    .catch(err => console.log('error populating users', err));
});


app.post("/api/newUser", function(req, res){
    var mod = new model(req.body);
    mod.save(function(err,data){
        if(err){
            return res.status(404).send(err);
        }
        else {
            return res.status(200).send({data:"User has been created..!!"});
        }
    });
})

app.get("/api/getUsers", function(req, res){
    model.find({}, function(err, data){
        if(err){
            console.log('No data')
            return res.status(404).send(err)
        } 
        else {
            console.log('Data')
            console.log(data)
            return res.status(200).send(data)
        }
    })
})

app.post("/api/updateUser", function(req, res){
    model.findByIdAndUpdate(req.body._id, {name: req.body.name, lastName: req.body.lastName, email: req.body.email, password: req.body.password},
        function(err, data) {
            if (err) {
                console.log('Hubo un error');
                return res.send(err);
            } 
            else {
                return res.status(200).send({data: "User has been updated..!!"});
            }
        });
})


app.post("/api/deleteUser", function(req, res){
    model.remove({ _id: req.body._id}, function(err){
        if (err) {
            res.send(err);
        }
        else {
            res.status(200).send({data:"User has been deleted..!!"});
        }
    })
})

app.post("/api/login", function(req, res){
    model.findOne({email: req.body.email}, function(err, data){
        if(err){
            res.status(404).send(err).end();
        } else {
            console.log(data);
            if(data.password == req.body.password){
                console.log("match");
                return res.status(204).send({data: "Login completed"}).end();                
            } else {
                console.log("wrong");
                return res.status(500).send({data: "Not matched"}).end();
            }
        }
    })
})

app.listen(8080, function () {
    console.log('Server running on port 8080')
});