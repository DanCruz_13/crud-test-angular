import { Component, OnInit } from '@angular/core';
import { CommonService } from '../common.service';
import { User } from '../models/user';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private newService: CommonService) {};

  loginCredentials: User = new User();

  
  ngOnInit() {
    this.loginCredentials.email = '';
    this.loginCredentials.password = '';
  }

  loginUser(){
    if (this.loginCredentials.email !== ''){
      if (this.loginCredentials.password !== '') {
        console.log(this.loginCredentials.email);
        console.log(this.loginCredentials.password);
        this.newService.login(this.loginCredentials).then(data => {
          console.log(data);
          if (data == undefined){
            console.log('Exito')
          } else {
            console.log('Fracaso');
          }
        });
      } else  {
        Swal.fire(
          'Error',
          'Please fill the password',
          'error'
        )
      }
    } else {
      Swal.fire(
        'Error',
        'Please enter a valid email',
        'error'
      )
    }
  }
}
