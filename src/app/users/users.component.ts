import { Component, OnInit } from '@angular/core';
import { User } from '../models/user';
import { CommonService } from '../common.service';
import Swal from 'sweetalert2'


@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  constructor(private newService: CommonService) {}

  usersArray: User[] = [];

  selectedUser: User = new User();
  
  ngOnInit() {
    this.newService.getUsers().subscribe(data => {
      console.log(data);
      this.usersArray = [];
      data.forEach(element => {
        this.usersArray.push(element);
      });
      this.selectedUser = new User();      
    });
  }

  addOrEditUser() {
    
    if (this.selectedUser._id === 0) {
      
      this.selectedUser._id = null;
      this.newService.saveUser(this.selectedUser).then(data => {
        console.log(data);
        this.ngOnInit();
        Swal.fire(
          'Excelent!',
          'The user has been created..!!',
          'success'
        )
      });
    } else {
      
      this.newService.updateUser(this.selectedUser).then(data => {
        console.log(data);
        this.ngOnInit();
        Swal.fire(
          'Excelent!',
          'The user has been updated..!!',
          'success'
        )
      });
    }
  }

  openForEdit(user) {
    this.selectedUser = user;
  }

  deleteUser() {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.value) {
        this.newService.deleteUser(this.selectedUser).then(data => {
          console.log(data);
          this.ngOnInit();
          Swal.fire(
            'Deleted!',
            'The user has been deleted..!!',
            'success'
          )
        });
      }
    })
  }


}
