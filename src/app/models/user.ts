export class User {
    _id: number = 0;
    name: string;
    lastName: string;
    email: string;
    password: string;
}
