import { Injectable } from '@angular/core';

import {HttpResponse, HttpHeaders, HttpRequest, HttpClient } from '@angular/common/http';

import { jsonpCallbackContext } from '@angular/common/http/src/module';
import { User } from './models/user';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor(private http: HttpClient) { }

  saveUser(user) {
    return this.http.post('http://localhost:8080/api/newUser', user).forEach((response: Response) => response);
  }

  updateUser(user) {
    return this.http.post('http://localhost:8080/api/updateUser', user).forEach((response: Response) => response);
  }

  deleteUser(user) {
    return this.http.post('http://localhost:8080/api/deleteUser', user).forEach((response: Response) => response);
  }
  
  getUsers() {
    return this.http.get<User>('http://localhost:8080/api/getUsers');
  }

  login(credentials) {
    return this.http.post('http://localhost:8080/api/login', credentials).forEach((response: Response) => response);
  }
}
